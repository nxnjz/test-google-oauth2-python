#!/usr/bin/env python3


from flask import Flask, redirect, request
import sys, json
import requests
app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello, World!'


def get_flow():
    from google_auth_oauthlib.flow import Flow
    flow = Flow.from_client_secrets_file(
            'client_secret.json',
            scopes=['profile', 'email','openid'],
            redirect_uri='https://oauth.dev.nxnjz.net/2')
    return flow


@app.route('/1')
def oauth1():
    flow = get_flow()
    print(flow.authorization_url(), file=sys.stderr)
    return redirect(flow.authorization_url()[0], code=302)


@app.route('/2')
def oauth2():
    flow = get_flow()
    flow.fetch_token(code=request.args.get('code'))
    #service = build('calendar', 'v3', credentials=credentials)
    token = flow.credentials.token
    userinfo = requests.get("https://openidconnect.googleapis.com/v1/userinfo", headers={"Authorization": "Bearer " + token})
    return 'token fetched: %s\nUserInfo: %s' % (token, userinfo.text)

def openid_config():
    return requests.get("https://accounts.google.com/.well-known/openid-configuration").json()

def client_config():
    return json.load(open("client_secret2.json"))

@app.route('/nolib/1')
def nolib_1():
    client = client_config()
    uri = client["web"]["auth_uri"] + "?" + "client_id=" + client["web"]["client_id"] + "&response_type=code" + "&scope=profile email openid" + "&redirect_uri=" + client["web"]["redirect_uris"][0] + "&state=not_a_real_token" + "&nonce=not_a_real_nonce"
    return redirect(uri, code=302)
#return str(client)

@app.route('/nolib/2')
def nolib_2():
    config = openid_config()
    client=client_config()
    token_endpoint = config["token_endpoint"]
    r = requests.post(token_endpoint, data={"code": request.args.get("code"), "client_id": client["web"]["client_id"], "client_secret": client["web"]["client_secret"], "redirect_uri":client["web"]["redirect_uris"][0], "grant_type":"authorization_code"})
    return r.text
    #print(config["token_endpoint"], file=sys.stderr)

    #return config


